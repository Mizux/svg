#!/usr/bin/env bash

set -x
apt-get update -qq
apt-get install -qq build-essential \
	gcc make cmake \
	doxygen graphviz \
	qt5-default qt3d5-dev qt3d-assimpsceneimport-plugin \
	#clang

