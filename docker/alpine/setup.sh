#!/usr/bin/env sh

set -x
apk add --no-cache \
	g++ make cmake \
	doxygen graphviz \
	qt5-qtbase-dev \
	#clang

