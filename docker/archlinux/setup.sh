#!/usr/bin/env bash

set -x
pacman -Sy --needed --noconfirm \
	gcc make cmake \
	doxygen graphviz \
	qt5-base qt5-3d \
  #clang

