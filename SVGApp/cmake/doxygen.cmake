# Generates doxygen documentation for TARGET.
# Details:
# - Creates a target ${TARGET}_doc (and add it .PHONY all)
# - Following parameter will be added as Target dependencies.
# Sample:
#  add_library(${PROJECT_NAME})
#  ...
#  include(doxygen)
#  add_documentation(${PROJECT_NAME})
function(add_documentation TARGET)
	find_package(Doxygen)
	if(DOXYGEN_FOUND)
		message(STATUS "${TARGET}: Found documentation tool: ${DOXYGEN_EXECUTABLE}")
		# Find all specific doxygen files
		file(GLOB_RECURSE _SRCS "*.dox" "*.dot" "*.md" "*.pu")
		#but remove anything from build dir
		file(GLOB_RECURSE RM_SRCS "${CMAKE_BINARY_DIR}/*")
		list(REMOVE_ITEM _SRCS ${RM_SRCS})
		
		set(DOC_OUTPUT_DIR ${CMAKE_BINARY_DIR}/usr/share/doc/${TARGET})
		configure_file(${CMAKE_CURRENT_SOURCE_DIR}/Doxyfile.in ${CMAKE_CURRENT_BINARY_DIR}/Doxyfile @ONLY)
		add_custom_target(${TARGET}_doc ALL
			DEPENDS ${TARGET}_doc_cmd
			SOURCES Doxyfile.in ${_SRCS}
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
			COMMENT "Target generating documentation for ${TARGET} using doxygen" VERBATIM)
		add_custom_command(OUTPUT ${TARGET}_doc_cmd ${DOC_OUTPUT_DIR}
			DEPENDS ${ARGN}
			COMMAND ${CMAKE_COMMAND} -E make_directory "${DOC_OUTPUT_DIR}"
			COMMAND ${DOXYGEN_EXECUTABLE} Doxyfile
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
			COMMENT "Command generating documentation for ${TARGET} using doxygen" VERBATIM)
		install(DIRECTORY ${DOC_OUTPUT_DIR} DESTINATION usr/share/doc/${TARGET})
	else()
		message(FATAL_ERROR "${TARGET}: Can't find documentation tool (i.e. doxygen) !")
	endif()
endfunction()
