//! @file
#pragma once

#include <QComboBox>
#include <QPushButton>
#include <QSpinBox>
#include <QTableWidget>
#include <QWidget>

//! @brief Holds main widget.
class GeneratorWidget : public QWidget {
  Q_OBJECT

public:
  //! @param[in] parent Parent object if any.
  GeneratorWidget(QWidget *parent = 0);
  virtual ~GeneratorWidget();

public slots:
  //! @brief This slot will calculate the stats.
  void generateSVG();

private:
  //! @brief Holds the calculate button.
  QPushButton *_selectFileButton;

  //! @brief Initializes the widget.
  void _setupWidget();

  //! @brief Writes svg header (i.e. \<svg...>)
  //! @param[out] out Output stream to fill.
  void writeHeader(std::ostream &out);

  //! @brief Writes svg definitions (i.e. \<def>...\</def>)
  //! @param[out] out Output stream to fill.
  void writeDefs(std::ostream &out);

  //! @brief Wrties svg footer (i.e. @</svg>@)
  //! @param[out] out Output stream to fill.
  void writeFooter(std::ostream &out);
};
