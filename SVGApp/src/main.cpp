//! @file
#include "GeneratorWidget.hpp"
#include <QApplication>

//! @brief Entry point of the program.
//! @param[in] argc The number of arguments.
//! @param[in] argv The vector of arguments.
//! @return an integer 0 upon exit success.
int main(int argc, char **argv) {
  QApplication app(argc, argv);
  app.setOrganizationName("Mizux");
  app.setApplicationName("SVGGenerator");

  GeneratorWidget main;
  main.show();

  return app.exec();
}
