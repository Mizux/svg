//! @file

#include "GeneratorWidget.hpp"

#include <QFileDialog>
#include <fstream>

#include <QHBoxLayout>
#include <QLabel>
#include <QSpacerItem>
#include <QVBoxLayout>
#include <cassert>
#include <iostream>
#include <numeric>

GeneratorWidget::GeneratorWidget(QWidget *parent) : QWidget(parent) {
  _setupWidget();
}

GeneratorWidget::~GeneratorWidget() {}

void GeneratorWidget::generateSVG() {
  QString fileName = QFileDialog::getSaveFileName(this, "Save svg", ".",
                                                  "Image Files (*.svg)");

  std::ofstream fs;
  fs.open(fileName.toStdString(), std::ios_base::out | std::ios_base::trunc);
  if (!fs)
    return;

  writeHeader(fs);

  writeDefs(fs);

  writeFooter(fs);

  fs.close();
}

void GeneratorWidget::_setupWidget() {
  setObjectName("SVGGenerator");
  setLayout(new QVBoxLayout(this));

  _selectFileButton = new QPushButton("Save");
  connect(_selectFileButton, &QPushButton::clicked, this,
          &GeneratorWidget::generateSVG);

  layout()->addWidget(_selectFileButton);
}

void GeneratorWidget::writeHeader(std::ostream &out) {
  out << "<svg "
      << "xmlns=\"http://www.w3.org/2000/svg\" "
      << "width=\"100%\" height=\"100%\""
      << ">" << std::endl;
}

void GeneratorWidget::writeDefs(std::ostream &out) {
  out << "<defs>" << std::endl;
  out << "</defs>" << std::endl;
}

void GeneratorWidget::writeFooter(std::ostream &out) {
  out << "</svg>" << std::endl;
}
