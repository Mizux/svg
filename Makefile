help:
	@echo "usage:"
	@echo "make docker: generate docker images"
	@echo "make configure: cmake configure"
	@echo "make build: cmake build"
	@echo "make test: run unit tests"
	@echo "make clean: remove build directories"
	@echo "make mrproprer: clean and also remove docker images"
	@echo ""
	@echo "note: you can use XXX_platform to generate target for a specific platform"
	@echo "supported platform:"
	@echo "  alpine, ubuntu, archlinux"

.PHONY: all clean mrproper help \
	docker docker_alpine docker_archlinux docker_ubuntu \
	configure configure_alpine configure_archlinux configure_ubuntu \
	build build_alpine build_archlinux build_ubuntu \
	test test_alpine test_archlinux test_ubuntu \
	clean clean_alpine clean_archlinux clean_ubuntu \
	mrproper mrproper_alpine mrproper_archlinux mrproper_ubuntu \

all: | mrproper docker configure build

UID := $(shell id -u)
GID := $(shell id -g)

# DOCKER
docker: docker_alpine docker_ubuntu docker_archlinux
docker_alpine: alpine.tar
docker_archlinux: archlinux.tar
docker_ubuntu: ubuntu.tar

%.tar: docker/%
	docker rmi -f project:$*
	docker build -t project:$* $<
	docker save project:$* -o $@

# CONFIGURE
configure: configure_alpine configure_ubuntu configure_archlinux
configure_alpine: | clean_alpine build-alpine
configure_archlinux: | clean_archlinux build-archlinux
configure_ubuntu: | clean_ubuntu build-ubuntu

build-%: %.tar
	mkdir -p $@
	docker load -i $<
	docker run --rm -v ${PWD}:/tmp -w /tmp/$@ --user ${UID}:${GID} project:$* /bin/sh -c "cmake .."

# BUILD
# $* stem
# $< first prerequist
# $@ target name
build: build_alpine build_ubuntu build_archlinux
build_alpine: alpine.tar build-alpine
	docker load -i $<
	docker run --rm -v ${PWD}:/tmp -w /tmp/build-alpine --user ${UID}:${GID} project:alpine /bin/sh -c "make"
build_archlinux: archlinux.tar build-archlinux
	docker load -i $<
	docker run --rm -v ${PWD}:/tmp -w /tmp/build-archlinux --user ${UID}:${GID} project:archlinux /bin/sh -c "make"
build_ubuntu: ubuntu.tar build-ubuntu
	docker load -i $<
	docker run --rm -v ${PWD}:/tmp -w /tmp/build-ubuntu --user ${UID}:${GID} project:ubuntu /bin/sh -c "make"

# TEST
test: test_alpine test_ubuntu test_archlinux
test_alpine: build_alpine
	docker load -i alpine.tar
	docker run --rm -v ${PWD}:/tmp -w /tmp/build-alpine --user ${UID}:${GID} project:alpine /bin/sh -c "ctest --output-on-failure"
test_archlinux: build_archlinux
	docker load -i archlinux.tar
	docker run --rm -v ${PWD}:/tmp -w /tmp/build-archlinux --user ${UID}:${GID} project:archlinux /bin/sh -c "ctest --output-on-failure"
test_ubuntu: build_ubuntu
	docker load -i ubuntu.tar
	docker run --rm -v ${PWD}:/tmp -w /tmp/build-ubuntu --user ${UID}:${GID} project:ubuntu /bin/sh -c "ctest --output-on-failure"

# CLEAN
clean: clean_alpine clean_ubuntu clean_archlinux
clean_alpine:
	rm -rf build-alpine
clean_archlinux:
	rm -rf build-archlinux
clean_ubuntu:
	rm -rf build-ubuntu

# MRPROPER
mrproper: mrproper_alpine mrproper_ubuntu mrproper_archlinux
mrproper_alpine: clean_alpine
	docker rmi -f project:alpine
	rm -f alpine.tar
mrproper_archlinux: clean_archlinux
	docker rmi -f project:archlinux
	rm -f archlinux.tar
mrproper_ubuntu: clean_ubuntu
	docker rmi -f project:ubuntu
	rm -f ubuntu.tar

